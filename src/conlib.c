#include "conlib.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>
#include <assert.h>
#include <time.h>
#include <windows.h>

// 内部静态变量声明
static HANDLE  hStdOut;										// 控制台输出句柄
static CONSOLE_SCREEN_BUFFER_INFOEX	csbiexOriginInfoex;		// 原始屏幕缓冲区信息
static CONSOLE_SCREEN_BUFFER_INFOEX	csbiexCurrInfoex;		// 当前屏幕缓冲区信息
static CONSOLE_CURSOR_INFO	cciOriginInfo;					// 原始光标信息
static CONSOLE_CURSOR_INFO	cciCurrInfo;					// 当前光标信息

static bool	bIsInitiate = false;							// 初始化状态

// 内部静态变量
static FILE* fpLog;											// 日志文件指针

// 内部静态函数声明
static int	conInitLog(const char* _strLog);
static bool	conIsOpenFile(void);
static bool	conIsEmptyFile(void);
static void	conExitLog(void);
static void	conWriteLog(const char* _strMessage, long _nErrorCode);

// 通用函数声明
int		conGetTime(struct tm* _pstTime)
{
	time_t	t_time;
	t_time = time(NULL);
	return	localtime_s(_pstTime, &t_time);
}

void	conSleep(long _uInterval)
{
	assert(_uInterval > 0);
	clock_t tick = 0;
	for (clock_t tick_base = clock(); tick < tick_base + _uInterval;)
	{
		tick = clock();
	}
}

uint	conRandNum(uint _min, uint _max)
{
	static uint	rand_num = 0;

	assert((_min > 0) && (_min < RAND_MAX) && (_max > 0) && (_max < RAND_MAX));
	do
	{
		srand((unsigned int)time(NULL) + rand());
		rand_num = rand() % _max;
		if (rand_num > _min)
		{
			break;
		}
	} while (1);

	return	rand_num;
}

// 库函数定义
int     conInitConsole(uint _uCols, uint _uRows)
{
	static bool    bRun = false;

	// 初始化日志函数
	conInitLog("Log.txt");
	if (!conIsOpenFile())
	{
		return  -1;
	}

	// 避免二次初始化
	if (!bRun)
	{
		// 获取控制台输出句柄
		hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
		if (!hStdOut)
		{
			conWriteLog("Could not Get the HANDLE to STD_OUTPUT_HANDLE!", GetLastError());
			return  -1;
		}

		// 获取屏幕缓冲区信息
		csbiexOriginInfoex.cbSize = sizeof(CONSOLE_SCREEN_BUFFER_INFOEX);
		if (!GetConsoleScreenBufferInfoEx(hStdOut, &csbiexOriginInfoex))
		{
			conWriteLog("Could not Get the INFOEX of the Console Screen Buffer!", GetLastError());
			return  -1;
		}
		csbiexCurrInfoex = csbiexOriginInfoex;
		
		// 设置窗口大小(0, 0, _uCols, _uRows)
		csbiexCurrInfoex.srWindow.Left = 0;
		csbiexCurrInfoex.srWindow.Top = 0;
		csbiexCurrInfoex.srWindow.Right = _uCols - 1;
		csbiexCurrInfoex.srWindow.Bottom = _uRows - 1;

		//// 设置屏幕缓冲区大小(_uCols, _uRows)
		//csbiexCurrInfoex.dwSize.X = _uCols;
		//csbiexCurrInfoex.dwSize.Y = _uRows + 1;

		// 设置光标位置(0, 0)
		csbiexCurrInfoex.dwCursorPosition.X = 0;
		csbiexCurrInfoex.dwCursorPosition.Y = 0;

		// 禁止窗口全屏
		csbiexCurrInfoex.bFullscreenSupported = false;

		// 设置屏幕缓冲区信息
		if (!SetConsoleScreenBufferInfoEx(hStdOut, &csbiexCurrInfoex))
		{
			conWriteLog("Failed to Set the INFOEX of the Console Screen Buffer!", GetLastError());
			return  -1;
		}

		// 获取光标信息
		if (!GetConsoleCursorInfo(hStdOut, &cciOriginInfo))
		{
			conWriteLog("Could not Get the INFO of the Console Cursor!", GetLastError());
			return  -1;
		}
		cciCurrInfo = cciOriginInfo;

		bIsInitiate = true;
		bRun = true;
	}

	return  0;
}

bool	conIsInitialConsole(void)
{
	return	bIsInitiate;
}

int     conExitConsole(void)
{
	static bool    bRun = false;

	// 避免被二次调用
	if (!bRun)
	{
		// 判断控制台是否已经初始化
		if (!conIsInitialConsole())
		{
			conWriteLog("The Console was not Initiated!", GetLastError());
			return	-1;
		}

		// 恢复屏幕缓冲区信息
		if (!SetConsoleScreenBufferInfoEx(hStdOut, &csbiexOriginInfoex))
		{
			conWriteLog("Failed to Set the INFOEX of the Console Screen Buffer!", GetLastError());
			return  -1;
		}

		// 退出日志
		conExitLog();

		bIsInitiate = false;
		bRun = true;
	}
	return  0;
}

int		conClearScreen(void)
{
	bool	bRet = false;
	COORD	dwOrigin = { 0, 0 };
	DWORD	dwChars;
	DWORD	dwCharsWritten;

	// 判断是否初始化
	if (!conIsInitialConsole())
	{
		conWriteLog("The Console was not Initiated!", GetLastError());
		return	-1;
	}

	// 计算清屏字符数
	dwChars = csbiexCurrInfoex.dwSize.X * csbiexCurrInfoex.dwSize.Y;

	// 填充屏幕缓冲区字符
	bRet = FillConsoleOutputCharacter(hStdOut, ' ', dwChars, dwOrigin, &dwCharsWritten);
	if (!bRet)
	{
		conWriteLog("Could not Fill Characters into the Output of the Console!", GetLastError());
		return	-1;
	}

	// 填充屏幕缓冲区字符属性
	bRet = FillConsoleOutputAttribute(hStdOut, csbiexCurrInfoex.wAttributes, dwChars, dwOrigin, &dwCharsWritten);
	if (!bRet)
	{
		conWriteLog("Could not Fill Attributes into the Output of the Console!", GetLastError());
		return	-1;
	}

	// 重置光标位置
	bRet = SetConsoleCursorPosition(hStdOut, dwOrigin);
	if (!bRet)
	{
		conWriteLog("Failed to Set the Position of the Console Cursor!", GetLastError());
		return	-1;
	}
	return	true;
}

int     conSetTextAttributes(uint _uTextAttributes)
{
	// 判断是否初始化
	if (!conIsInitialConsole())
	{
		conWriteLog("The Console was not Initiated!", GetLastError());
		return	-1;
	}

	// 如果传入参数为0则恢复原始屏幕缓冲区信息属性，
	//   否则设置新的屏幕缓冲区信息属性
	if (_uTextAttributes == 0)
	{
		csbiexCurrInfoex = csbiexOriginInfoex;
	}
	else
	{
		csbiexCurrInfoex.wAttributes = _uTextAttributes;
	}

	if (!SetConsoleScreenBufferInfoEx(hStdOut, &csbiexCurrInfoex))
	{
		conWriteLog("Failed to Set the INFOEX of the Console Screen Buffer!", GetLastError());
		return  -1;
	}
	return  0;
}

int     conGetTextAttributes(uint* _puTextAttributes)
{
	// 判断是否初始化
	if (!conIsInitialConsole())
	{
		conWriteLog("The Console was not Initiated!", GetLastError());
		return	-1;
	}

	*_puTextAttributes = csbiexCurrInfoex.wAttributes;
	return	0;
}

int     conShowCursor(void)
{
	// 判断是否初始化
	if (!conIsInitialConsole())
	{
		conWriteLog("The Console was not Initiated!", GetLastError());
		return	-1;
	}

	if (!cciCurrInfo.bVisible)
	{
		cciCurrInfo.bVisible = true;
		if (!SetConsoleCursorInfo(hStdOut, &cciCurrInfo))
		{
			conWriteLog("Failed to Set the INFO of the Console Cursor!", GetLastError());
			return  -1;
		}
	}
	return	0;
}

int     conHideCursor(void)
{
	// 判断是否初始化
	if (!conIsInitialConsole())
	{
		conWriteLog("The Console was not Initiated!", GetLastError());
		return	-1;
	}

	if (cciCurrInfo.bVisible)
	{
		cciCurrInfo.bVisible = false;
		if (!SetConsoleCursorInfo(hStdOut, &cciCurrInfo))
		{
			conWriteLog("Failed to Set the INFO of the Console Cursor!", GetLastError());
			return  -1;
		}
	}
	return	0;
}

bool    conIsVisibleCursor(void)
{
	// 判断是否初始化
	if (!conIsInitialConsole())
	{
		conWriteLog("The Console was not Initiated!", GetLastError());
		return	-1;
	}

	return	cciCurrInfo.bVisible;
}

int     conSetCursorSize(uint _uCursorSize)
{
	// 判断是否初始化
	if (!conIsInitialConsole())
	{
		conWriteLog("The Console was not Initiated!", GetLastError());
		return	-1;
	}

	assert((_uCursorSize >= 1) && (_uCursorSize <= 100));
	cciCurrInfo.dwSize = _uCursorSize;
	if (!GetConsoleCursorInfo(hStdOut, &cciCurrInfo))
	{
		conWriteLog("Could not Get the Console Cursor!", GetLastError());
		return  -1;
	}

	// 设置光标尺寸

	return	0;
}

uint    conGetCursorSize(void)
{
	// 判断是否初始化
	if (!conIsInitialConsole())
	{
		conWriteLog("The Console was not Initiated!", GetLastError());
		return	-1;
	}

	return	cciCurrInfo.dwSize;
}

int     conSetCursorPosition(uint _uX, uint _uY)
{
	// 判断是否初始化
	if (!conIsInitialConsole())
	{
		conWriteLog("The Console was not Initiated!", GetLastError());
		return	-1;
	}

	// 判断鼠标位置是否合法
	assert((_uX >= 0) && (_uY >= 0));

	// 获取最新屏幕缓冲区信息
	if (!GetConsoleScreenBufferInfoEx(hStdOut, &csbiexCurrInfoex))
	{
		conWriteLog("Could not Get the INFOEX of the Console Screen Buffer!", GetLastError());
		return  -1;
	}

	// 更新光标位置信息
	csbiexCurrInfoex.dwCursorPosition.X = _uX;
	csbiexCurrInfoex.dwCursorPosition.Y = _uY;
	if (!SetConsoleCursorPosition(hStdOut, csbiexCurrInfoex.dwCursorPosition))
	{
		conWriteLog("Failed to Set the Position of the Console Cursor!", GetLastError());
		return  -1;
	}
	return	0;
}

int		conGetCursorPosition(uint* _puX, uint* _puY)
{
	// 判断是否初始化
	if (!conIsInitialConsole())
	{
		conWriteLog("The Console was not Initiated!", GetLastError());
		return	-1;
	}

	// 获取最新屏幕缓冲区信息
	if (!GetConsoleScreenBufferInfoEx(hStdOut, &csbiexCurrInfoex))
	{
		conWriteLog("Could not Get the INFOEX of the Console Screen Buffer!", GetLastError());
		return  -1;
	}
	*_puX = csbiexCurrInfoex.dwCursorPosition.X;
	*_puY = csbiexCurrInfoex.dwCursorPosition.Y;
	return	0;
}

int		conPutCharWithAttributes(uint _uTextAttributes, int _iChar)
{
	uint	uCurTextAttribs;

	// 判断是否初始化
	if (!conIsInitialConsole())
	{
		conWriteLog("The Console was not Initiated!", GetLastError());
		return	-1;
	}

	// 获取当前控制台屏幕缓冲区信息属性
	conGetTextAttributes(&uCurTextAttribs);

	// 修改控制台屏幕缓冲区信息属性
	conSetTextAttributes(_uTextAttributes);

	// 输出字符
	putchar(_iChar);

	// 恢复控制台缓冲区信息
	conSetTextAttributes(uCurTextAttribs);
	return	0;
}

int		conPrintWithAttributes(uint _uTextAttributes, const char* _Format, ...)
{
	int	arg_count;
	uint	uCurTextAttribs;

	// 判断是否初始化
	if (!conIsInitialConsole())
	{
		conWriteLog("The Console was not Initiated!", GetLastError());
		return	-1;
	}

	// 获取当前控制台屏幕缓冲区信息属性
	conGetTextAttributes(&uCurTextAttribs);

	// 修改控制台屏幕缓冲区信息属性
	conSetTextAttributes(_uTextAttributes);

	// 调用标准输出函数
	va_list ap;
	va_list apcopy;

	va_start(ap, _Format);
	va_copy(apcopy, ap);

	arg_count = vprintf(_Format, ap);
	va_end(ap);

	// 恢复控制台缓冲区信息
	conSetTextAttributes(uCurTextAttribs);

	return  arg_count;
}

// 内部静态函数定义，仅供其他函数内部调用，不开放对外接口！
static int	conInitLog(const char* _strLog)
{
	struct tm	stTime;

	// 判断fpLog文件指针是否已经打开
	if (fpLog)
	{
		// 判断文件是否为空
		if (conIsEmptyFile())
		{
			conGetTime(&stTime);
			fprintf_s(fpLog, "[NOTICE]: The Log system has been initiated! ");
			fprintf_s(fpLog, "Don't Initiate it again! %04d-%02d-%02d, %02d:%02d:%02d\n",
				stTime.tm_year + 1900, stTime.tm_mon, stTime.tm_mday,
				stTime.tm_hour, stTime.tm_min, stTime.tm_sec);
		}
		return	0;
	}

	// 向_strLog的日志文件追加写入错误信息，如果文件找不到则新建
	fopen_s(&fpLog, _strLog, "a+t");
	if (!fpLog)
	{
		printf("\nFailed to Open Log File!\n");
		printf("  [Line]: %d\n", __LINE__);
		printf("  [File]: %s\n", __FILE__);
		return	-1;
	}

	// 初始化成功后判断是否为空，如为空则写入第1条记录
	if (conIsEmptyFile())
	{
		conGetTime(&stTime);
		fprintf_s(fpLog, "[LOG START] - DATE: %04d-%02d-%02d, TIME: %02d:%02d:%02d\n",
			stTime.tm_year + 1900, stTime.tm_mon, stTime.tm_mday,
			stTime.tm_hour, stTime.tm_min, stTime.tm_sec);
	}

	return	0;
}

static bool	conIsOpenFile(void)
{
	if (!fpLog)
	{
		return	false;
	}
	
	return	true;
}

static bool	conIsEmptyFile(void)
{
	long	nLen, nLast;

	// 判断文件是否打开
	if (!conIsOpenFile())
	{
		printf("\nFailed to Open Log File!\n");
		printf("  [Line]: %d\n", __LINE__);
		printf("  [File]: %s\n", __FILE__);
		return	false;
	}
	
	// 获取文件大小
	fseek(fpLog, 0L, SEEK_SET);
	nLen = ftell(fpLog);
	fseek(fpLog, 0L, SEEK_END);
	nLast = ftell(fpLog);
	nLen = nLast - nLen;

	return	!nLen;
}

static void	conExitLog(void)
{
	// 关闭文件
	if (fclose(fpLog))
	{
		printf("\nFailed to Close Log File!\n");
		printf("  [Line]: %d\n", __LINE__);
		printf("  [File]: %s\n", __FILE__);
	}
}

static void	conWriteLog(const char* _strMessage, long _nErrorCode)
{
	struct tm	stTime;

	if (!conIsOpenFile())
	{
		printf("\nFailed to Write Log to File!\n");
		printf("  [Line]: %d\n", __LINE__);
		printf("  [File]: %s\n", __FILE__);
	}

	conGetTime(&stTime);
	fprintf_s(fpLog, "[ERROR]: %s, [Line]: %d, [File]: %s, ", _strMessage, __LINE__, __FILE__);
	fprintf_s(fpLog, "[Error Code]: 0x%08x,  %04d-%02d-%02d, %02d:%02d:%02d\n",
		_nErrorCode, stTime.tm_year + 1900, stTime.tm_mon, stTime.tm_mday,
		stTime.tm_hour, stTime.tm_min, stTime.tm_sec);
}
