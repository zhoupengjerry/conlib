#ifndef CONLIB_H
#define CONLIB_H

// 引用声明
#include	<windows.h>
#include	<stdbool.h>

// 接口定义：包括数据类型定义、宏定义和库函数声明
//// 数据类型定义
typedef unsigned int	uint;

//// 库函数声明
////// 通用函数声明，以下函数调用不涉及库函数引用
int		conGetTime(struct tm* _pstTime);
void	conSleep(long _uInterval);
uint	conRandNum(uint _min, uint _max);

////// 库函数声明，以下函数调用需要在调用初始化函数conInitConsole调用之后，
//////   退出函数conExitConsole调用之前使用
int     conInitConsole(uint _uCols, uint _uRows);
bool	conIsInitialConsole(void);
int     conExitConsole(void);
int		conClearScreen(void);

int     conSetTextAttributes(uint _uTextAttributes);
int     conGetTextAttributes(uint* _puTextAttributes);

int     conShowCursor(void);
int     conHideCursor(void);
bool    conIsVisibleCursor(void);

int     conSetCursorSize(uint _uCursorSize);
uint    conGetCursorSize(void);

int     conSetCursorPosition(uint _uX, uint _uY);
int     conGetCursorPosition(uint* _puX, uint* _puY);

int		conPutCharWithAttributes(uint _uTextAttributes, int _iChar);
int		conPrintWithAttributes(uint _uTextAttributes, const char* _Format, ...);

#endif  // !conlib.h
