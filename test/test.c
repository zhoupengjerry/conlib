/**
 * Test.c -- 测试ConLib库
 */

#include    "conlib.h"
#include	<stdio.h>
#include	<conio.h>
#include	<time.h>

 //#define	DBL_CORN_BOTTOMLEFT		0x00C8		//
 //#define	DBL_CORN_TOPLEFT		0x00C9		//
 //#define	DBL_CORN_TOPRIGHT		0x00BB		//
 //#define	DBL_CORN_BOTTOMRIGHT	0x00BC		//

 //#define	DBL_HLINE				0x00CD		//
 //#define	DBL_VLINE				0x00BA		//

#define	DBL_CORN_BOTTOMLEFT		'+'		//
#define	DBL_CORN_TOPLEFT		'+'		//
#define	DBL_CORN_TOPRIGHT		'+'		//
#define	DBL_CORN_BOTTOMRIGHT	'+'		//

#define	DBL_HLINE				'-'		//
#define	DBL_VLINE				'|'		//

void	DrawWindow(uint _uLeft, uint _uTop, uint _uRight, uint _uBottom);

int main()
{
	uint	uStyleGeneral = FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE;
	uint	uStyleHighlight = FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_INTENSITY;
	uint	uStyleRedHighlight = FOREGROUND_RED | FOREGROUND_INTENSITY;
	uint	uStyleGreenHighlight = FOREGROUND_GREEN | FOREGROUND_INTENSITY;
	uint	uStyleBlueHighlight = FOREGROUND_BLUE | FOREGROUND_INTENSITY;
	uint	uStyleYelloHighlight = FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_INTENSITY;
	uint	uStylePinkHighlight = FOREGROUND_RED | FOREGROUND_BLUE | FOREGROUND_INTENSITY;

	struct tm	stTime;

	conInitConsole(100, 40);
	conClearScreen();
	conHideCursor();
	conSetTextAttributes(uStyleGreenHighlight);
	conSetCursorPosition(1, 1);
	DrawWindow(9, 1, 89, 25);
	conSetTextAttributes(uStyleYelloHighlight);
	conSetCursorPosition(25, 2);
	printf("Copyright (c) by Catsoft (R) Studio.");
	conSetCursorPosition(30, 4);
	printf("All rights preserved, 2019.");
	conSetCursorPosition(10, 6);
	conSetTextAttributes(uStyleRedHighlight);
	for (int i = 1; i < 80; i++)
	{
		printf("-");
	}

	conSetTextAttributes(uStylePinkHighlight);
	do
	{
		conSetCursorPosition(79, 2);
		conGetTime(&stTime);
		printf("%04d-%02d-%02d", stTime.tm_year + 1900, stTime.tm_mon, stTime.tm_mday);
		conSetCursorPosition(81, 3);
		printf("%02d:%02d:%02d", stTime.tm_hour, stTime.tm_min, stTime.tm_sec);
		conSleep(1000);
	} while (!_kbhit());

	conExitConsole();

	return  0;
}

void	DrawWindow(uint _uLeft, uint _uTop, uint _uRight, uint _uBottom)
{
	uint	uRow;

	// 打印四个角
	conSetCursorPosition(_uLeft, _uTop);
	printf("%c", DBL_CORN_TOPLEFT);
	conSetCursorPosition(_uRight, _uTop);
	printf("%c", DBL_CORN_TOPRIGHT);
	conSetCursorPosition(_uLeft, _uBottom);
	printf("%c", DBL_CORN_BOTTOMLEFT);
	conSetCursorPosition(_uRight, _uBottom);
	printf("%c", DBL_CORN_BOTTOMRIGHT);

	// 打印顶水平线
	conSetCursorPosition(_uLeft + 1, _uTop);
	for (uint i = 0; i < _uRight - _uLeft - 1; i++)
	{
		printf("%c", DBL_HLINE);
	}

	// 打印底部水平线
	conSetCursorPosition(_uLeft + 1, _uBottom);
	for (uint i = 0; i < _uRight - _uLeft - 1; i++)
	{
		printf("%c", DBL_HLINE);
	}

	// 打印左侧垂直线
	conSetCursorPosition(_uLeft, _uTop + 1);
	for (uRow = _uTop + 1; uRow < _uBottom; uRow++)
	{
		conSetCursorPosition(_uLeft, uRow);
		printf("%c", DBL_VLINE);
	}

	// 打印右侧垂直线
	conSetCursorPosition(_uRight, _uTop + 1);
	for (uRow = _uTop + 1; uRow < _uBottom; uRow++)
	{
		conSetCursorPosition(_uRight, uRow);
		printf("%c", DBL_VLINE);
	}
}