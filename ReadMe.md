# conLib库说明

## 基本思想

为了控制台编程更方便，采取封装Windows Console相关函数和数据结构的方式。对于诸如控制台的控制句柄（HANDLE）、涉及控制台屏幕缓冲区属性的某些常用的内部数据结构，封装在库实现的内部。

## 引用声明

```C
#include	<windows.h>
#include    <stdbool.h>
```

## 接口定义

### 1、数据类型定义

```C
typedef unsigned int	uint;
```

### 2、库函数声明

```C
int		conGetTime(struct tm* _pstTime);
void	conSleep(long _uInterval);
uint	conRandNum(uint _min, uint _max);

int     conInitConsole(uint _uCols, uint _uRows);
bool	conIsInitialConsole(void);
int     conExitConsole(void);
int		conClearScreen(void);

int     conSetTextAttributes(uint _uTextAttributes);
int     conGetTextAttributes(uint* _puTextAttributes);

int     conShowCursor(void);
int     conHideCursor(void);
bool    conIsVisibleCursor(void);

int     conSetCursorSize(uint _uCursorSize);
uint    conGetCursorSize(void);

int     conSetCursorPosition(uint _uX, uint _uY);
int     conGetCursorPosition(uint* _uX, uint* _uY);

int		conPutCharWithAttributes(uint _uTextAttributes, int _iChar);
int		conPrintWithAttributes(uint _uTextAttributes, const char* _Format, ...);
```

## 用例演示

### 用例1：



### 用例2：

